package com.example.andenginesample;

import android.graphics.Point;

import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.SimpleLayoutGameActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PuzzleScene extends BaseScene implements ITouchArea {

    private static final String STAGE_BG = "img/stage_bg.png";
    private static final String DROP_00 = "img/drop_00.png";
    private static final String DROP_01 = "img/drop_01.png";
    private static final String DROP_02 = "img/drop_02.png";
    private static final String DROP_03 = "img/drop_03.png";
    private static final String DROP_04 = "img/drop_04.png";
    private static final String DROP_05 = "img/drop_05.png";
    private static final String[] DROP_IMAGE = new String[] { DROP_00, DROP_01, DROP_02, DROP_03, DROP_04, DROP_05 };

    public static final int COL = 6;
    public static final int ROW = 5;

    class GameFlag {
        public static final int INITIALIZE = 0;
        public static final int MOVE_WAIT = 1;
        public static final int CHECK_WAIT = 2;
        public static final int ANIMATION_WAIT = 3;
        public static final int DROP_WAIT = 4;
    }

    private Sprite mBgSprite1;

    private Text mText;

    private Sprite[][] mDrops;

    private int mGameFlg = GameFlag.MOVE_WAIT;

    private Point mDownP;
    private Sprite mMoveDrop;

    private List<List<Point>> mChainList = new ArrayList<List<Point>>();

    private long removeDrops = 0;
    private int wait = 0;

    /**
     * コンストラクタ
     * @param activity
     */
    public PuzzleScene(SimpleLayoutGameActivity activity) {
        super(activity);
    }

    /**
     * 画面の生成処理
     */
    @Override
    public void onCreateScene() {
        mBgSprite1 = createSprite(STAGE_BG);
        attachChild(mBgSprite1);
        mDrops = new Sprite[COL][ROW];

        for (int i = 0; i < COL; i++) {
            for (int j = 0; j < ROW; j++) {
                Sprite sprite = createNextDrop();
                Point p = calcDropPosition(i, j);
                sprite.setPosition(p.x, p.y);
                mDrops[i][j] = sprite;
                attachChild(sprite);
            }
        }

        mText = createText("消した個数 : " + 0);
        mText.setPosition(50, 500);

        attachChild(mText);
        mGameFlg = GameFlag.MOVE_WAIT;
        this.registerTouchArea(this);
    }

    /**
     * ドロップを生成して返す。
     * 色はランダム
     * @return
     */
    private Sprite createNextDrop() {
        Random r = new Random();
        int n = r.nextInt(6);
        Sprite sprite = createSprite(DROP_IMAGE[n]);
        sprite.setTag(n);//ドロップの色をTagに入れておく（手抜きです通常はsetUserDataを使うと思う）

        return sprite;
    }

    /**
     * 移動時のみに表示するドロップを生成する
     * @param tag
     * @return
     */
    private Sprite createMoveDrop(int tag) {
        Sprite sprite = createSprite(DROP_IMAGE[tag]);
        return sprite;
    }

    private Point calcDropPosition(int col, int row) {
        Point point = new Point();

        float width = mAct.getEngine().getCamera().getWidth();
        float height = mAct.getEngine().getCamera().getHeight();

        int dropW = (int) (width / COL);
        int dropH = dropW;
        int top = (int) (height - dropH * ROW);
        int left = 0;

        point.x = left + dropW * col;
        point.y = top + dropH * row;
        return point;
    }

    /**
     * 1秒間に60回呼ばれる
     * スプライトの座標を移動させたり計算処理をする。
     */
    @Override
    public void update() {
        switch (mGameFlg) {
        case GameFlag.CHECK_WAIT:
            // 揃っている列行を調べる。
            if (mChainList.size() == 0) {
                checkChainDrop();
            }
            // そろっている行があれば消す。
            if (mChainList.size() != 0) {
                List<Point> pList = mChainList.get(0);
                mChainList.remove(0);
                for (Point p : pList) {
                    mDrops[p.x][p.y].setVisible(false);
                    removeDrops++;
                }
                wait = 30;
                mGameFlg = GameFlag.ANIMATION_WAIT;
            } else {
                // ドロップを下まで落とす。
                mGameFlg = GameFlag.DROP_WAIT;
                wait = 30;
            }
            break;
        case GameFlag.ANIMATION_WAIT:
            // アニメーションを待つ
            if (wait <= 0) {
                wait = 0;
                mGameFlg = GameFlag.CHECK_WAIT;
            } else {
                wait--;
            }
            break;
        case GameFlag.DROP_WAIT:
            // ドロップの落下
            if (wait <= 0) {
                checkChainDrop();
                if (mChainList.size() == 0) {
                    wait = 0;
                    mGameFlg = GameFlag.MOVE_WAIT;
                } else {
                    mGameFlg = GameFlag.CHECK_WAIT;
                }
            } else {
                wait--;
                updateDropDrop();
            }
            break;
        case GameFlag.MOVE_WAIT:
            break;
        default:
            break;
        }

        mText.setText("消した個数: " + removeDrops);
    }

    /**
     * ドロップのつながりをチェックする。
     */
    private void checkChainDrop() {

        List<List<Point>> chainList = new ArrayList<List<Point>>();

        for (int i = 0; i < COL; i++) {
            for (int j = 0; j < ROW; j++) {
                List<Point> pList = checkChainDrop(i,j);

                if (pList.size() != 0) {
                    chainList.add(pList);
                }
            }
        }

        mChainList = chainList;
    }

    /**
     * 作成中。
     */
    private void checkChainDrop2() {
        // 書きかけ（
        List<List<Point>> chainList = new ArrayList<List<Point>>();

        for (int i = 0; i < COL;i++) {
            for (int j = 0; j < ROW; j++) {
                try {
                    int target = mDrops[i][j].getTag();
                    if (target == mDrops[i + 1][j].getTag() && target == mDrops[i + 2][j].getTag()) {
                        List<Point> list = new ArrayList<Point>();
                        list.add(new Point(i,j));
                        list.add(new Point(i + 1,j));
                        list.add(new Point(i + 2,j));
                        chainList.add(list);
                    }
                    if (target == mDrops[i][j + 1].getTag() && target == mDrops[i][j + 2].getTag()) {
                        List<Point> list = new ArrayList<Point>();
                        list.add(new Point(i,j));
                        list.add(new Point(i,j + 1));
                        list.add(new Point(i,j + 2));
                        chainList.add(list);
                    }
                } catch (ArrayIndexOutOfBoundsException e) {

                }
            }
        }

        // 重複をチェックする
        List<List<Point>> tempList = new ArrayList<List<Point>>();

        for (List<Point> list1: chainList) {
            for (List<Point> list2: chainList) {
                if (mDrops[list1.get(0).x][list1.get(0).y].getTag() == mDrops[list2.get(0).x][list2.get(0).y].getTag() ) {
                    // 同じ色のドロップ
                    if (checkRelated(list1, list2)) {
                        // 隣接している。
                        List<Point> list = new ArrayList<Point>();
                        list.addAll(list1);
                        list.addAll(list2);
                        tempList.add(list);
                    }
                }
            }
        }

        // 重複を削除する。
        List<List<Point>> tempList2 = new ArrayList<List<Point>>();
        for (List<Point> list1: tempList) {
            for (int i = 0;i < chainList.size();i++) {
                List<Point> list2 = chainList.get(i);
                if (mDrops[list1.get(0).x][list1.get(0).y].getTag() == mDrops[list2.get(0).x][list2.get(0).y].getTag() ) {
                    // 同じ色のドロップ
                    if (checkOverlap(list1, list2)) {
                        // 重複している。
                        tempList2.add(list1);
                    } else {
//TODO
                    }
                }


            }
        }

    }

    /**
     * 重なりをチェックする。
     * @param list1
     * @param list2
     * @return
     */
    private boolean checkOverlap(List<Point> list1, List<Point> list2) {
        for (Point p1 : list1) {
            for (Point p2 : list2) {
                if (p1.x == p2.x && p1.y == p2.y) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 座標が隣接しているかどうかを返す。
     * @param list1
     * @param list2
     * @return
     */
    private boolean checkRelated(List<Point> list1, List<Point> list2) {
        for (Point p1 : list1) {
            for (Point p2 : list2) {
                if (p1.x == p2.x && p1.y == p2.y) {
                    return true;
                } else if (p1.x == p2.x && (p1.y == p2.y +1 || p1.y == p2.y -1)) {
                    return true;
                } else if (p1.y == p2.y && (p1.x == p2.x +1 || p1.x == p2.x -1)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 指定の座標に隣接する、3個以上連なった、同じ色のドロップのリストを返す
     * ↑せつめいややこい
     * @param px
     * @param py
     * @return
     */
    private List<Point> checkChainDrop(int px, int py) {
        List<Point> chainList = new ArrayList<Point>();

        List<Point> pList = new ArrayList<Point>();
        pList.add(new Point(px, py));
        int target = mDrops[px][py].getTag();

        if (target >= 10) {
            return new ArrayList<Point>();
        }

        if (px != COL -1) {
            for (int i = px + 1; i < COL;i++) {
                if (mDrops[i][py].getTag() == target) {
                    pList.add(new Point(i, py));
                } else {
                    break;
                }
            }
        }

        if (pList.size() >= 3) {
            for (Point p: pList) {
                chainList.add(p);
            }
        }

        // 上下を見る
        for (Point p : pList) {
            List<Point> tempList = new ArrayList<Point>();
            tempList.add(p);
            for (int j = p.y +1 ; j < ROW;j++) {
                if (mDrops[p.x][j].getTag() == target) {
                    tempList.add(new Point(p.x, j));
                } else {
                    break;
                }
            }
            for (int j = p.y - 1; j > -1; j--) {
                if (mDrops[p.x][j].getTag() == target) {
                    tempList.add(new Point(p.x, j));
                } else {
                    break;
                }
            }

            if (tempList.size() >= 3) {
                for (Point p2: tempList) {
                    chainList.add(p2);
                }
            }
        }

        //左右を見る
        pList = new ArrayList<Point>();
        for (Point p : chainList) {
            List<Point> tempList = new ArrayList<Point>();
            tempList.add(p);
            for (int i = p.x +1 ; i < COL;i++) {
                if (mDrops[i][p.y].getTag() == target) {
                    tempList.add(new Point(i, p.y));
                } else {
                    break;
                }
            }

            for (int i = p.x - 1; i > -1; i--) {
                if (mDrops[i][p.y].getTag() == target) {
                    tempList.add(new Point(i,p.y));
                } else {
                    break;
                }
            }

            if (tempList.size() >= 3) {
                for (Point p2: tempList) {
                    pList.add(p2);
                }
            } else {
                pList.add(p);
            }
        }

        // もう一度上下を見る
        for (Point p : pList) {
            List<Point> tempList = new ArrayList<Point>();
            tempList.add(p);
            for (int j = p.y +1 ; j < ROW;j++) {
                if (mDrops[p.x][j].getTag() == target) {
                    tempList.add(new Point(p.x, j));
                } else {
                    break;
                }
            }
            for (int j = p.y - 1; j > -1; j--) {
                if (mDrops[p.x][j].getTag() == target) {
                    tempList.add(new Point(p.x, j));
                } else {
                    break;
                }
            }

            if (tempList.size() >= 3) {
                for (Point p2: tempList) {
                    chainList.add(p2);
                }
            } else {
                chainList.add(p);
            }
        }

        //もう一度左右を見る
        pList = new ArrayList<Point>();
        for (Point p : chainList) {
            List<Point> tempList = new ArrayList<Point>();
            tempList.add(p);
            for (int i = p.x +1 ; i < COL;i++) {
                if (mDrops[i][p.y].getTag() == target) {
                    tempList.add(new Point(i, p.y));
                } else {
                    break;
                }
            }

            for (int i = p.x - 1; i > -1; i--) {
                if (mDrops[i][p.y].getTag() == target) {
                    tempList.add(new Point(i,p.y));
                } else {
                    break;
                }
            }

            if (tempList.size() >= 3) {
                for (Point p2: tempList) {
                    pList.add(p2);
                }
            } else {
                pList.add(p);
            }
        }


        List<Point> returnList = new ArrayList<Point>();
        for (Point p : pList) {
            if (mDrops[p.x][p.y].getTag() == target) {
                mDrops[p.x][p.y].setTag(target + 10);
                returnList.add(p);
            }
        }
        return returnList;
    }


    private void updateDropDrop() {
        // ドロップの落下

        boolean isDrop = false;
        for (int i = 0; i < COL; i++) {
            for (int j = 0; j < ROW - 1; j++) {
                if (mDrops[i][j].isVisible()) {
                    if (j != ROW - 1 && !mDrops[i][j + 1].isVisible()) {
                        Sprite sprite = mDrops[i][j];

                        sprite.setPosition(sprite.getX(), sprite.getY() + sprite.getHeight() / ROW);
                        isDrop = true;

                        if (sprite.getY() > mDrops[i][j + 1].getY()) {
                            Sprite tempSpr = mDrops[i][j + 1];
                            mDrops[i][j + 1] = sprite;
                            mDrops[i][j] = tempSpr;

                            Point p = calcDropPosition(i, j + 1);
                            mDrops[i][j + 1].setPosition(p.x, p.y);
                            p = calcDropPosition(i, j);
                            mDrops[i][j].setPosition(p.x, p.y);
                            isDrop = true;
                        }
                    }
                } else if (j == 0) {
                    Sprite newDrop = createNextDrop();
                    Sprite sprite = mDrops[i][j];
                    detachChild(sprite);
                    attachChild(newDrop);
                    Point p = calcDropPosition(i, j);
                    newDrop.setPosition(p.x, p.y);
                    mDrops[i][j] = newDrop;
                    isDrop = true;
                }

            }
        }

        if (!isDrop) {
            wait = 0;
        } else {
            wait = 30;
        }
    }

    /**
     * registerTouchArea()でセットしたスプライト範囲にタッチされた場合呼ばれる
     */
    @Override
    public boolean contains(float pX, float pY) {
        switch (mGameFlg) {
        case GameFlag.MOVE_WAIT:
            return true;
        default:
            return false;
        }
    }

    /**
     * containsにてtrueを返した場合にコールされる
     */
    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        // TODO 自動生成されたメソッド・スタブ
        // Log.i("onAreaTouched", pTouchAreaLocalX + " : " + pTouchAreaLocalY);

        int pX = (int) pSceneTouchEvent.getX();
        int pY = (int) pSceneTouchEvent.getY();

        if (pSceneTouchEvent.isActionUp()) {
            if (mDownP != null) {
                mDrops[mDownP.x][mDownP.y].setVisible(true);
                detachChild(mMoveDrop);
                mMoveDrop = null;
                mDownP = null;
                mGameFlg = GameFlag.ANIMATION_WAIT;
            }
            return true;
        }

        if (pSceneTouchEvent.isActionDown()) {

            for (int i = 0; i < COL; i++) {
                for (int j = 0; j < ROW; j++) {
                    Sprite sprite = mDrops[i][j];
                    if (sprite.contains(pX, pY)) {
                        mDownP = new Point(i, j);
                        sprite.setVisible(false);
                        mMoveDrop = createMoveDrop(sprite.getTag());
                        mMoveDrop.setPosition(pX - sprite.getWidth() / 2, pY - sprite.getHeight() / 2);
                        attachChild(mMoveDrop);
                        return true;
                    }
                }
            }
            mDownP = null;
        }

        if (pSceneTouchEvent.isActionMove() && mDownP != null) {
            Sprite sprite = mDrops[mDownP.x][mDownP.y];

            mMoveDrop.setPosition(pX - sprite.getWidth() / 2, pY - sprite.getHeight() / 2);

            if (!sprite.contains(pTouchAreaLocalX, pTouchAreaLocalY)) {

                for (int i = 0; i < COL; i++) {
                    for (int j = 0; j < ROW; j++) {
                        if (mDrops[i][j].contains(pX, pY)) {
                            Sprite tempSprite = mDrops[i][j];
                            mDrops[i][j] = sprite;
                            Point p = calcDropPosition(i, j);
                            sprite.setPosition(p.x, p.y);

                            mDrops[mDownP.x][mDownP.y] = tempSprite;

                            p = calcDropPosition(mDownP.x, mDownP.y);
                            tempSprite.setPosition(p.x, p.y);

                            mDownP = new Point(i, j);
                            return true;
                        }
                    }
                }
            }

        }
        return false;
    }


}