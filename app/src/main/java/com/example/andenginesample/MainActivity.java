package com.example.andenginesample;

import android.util.DisplayMetrics;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.SimpleLayoutGameActivity;

public class MainActivity extends SimpleLayoutGameActivity {
	private static final int CAMERA_WIDTH  = 128 * PuzzleScene.COL;
	private static final int CAMERA_HEIGHT = (int)(128 * PuzzleScene.COL / 9 * 16);


	@Override
	public EngineOptions onCreateEngineOptions() {

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		int height = (int)(1.0f * metrics.heightPixels / metrics.widthPixels * CAMERA_WIDTH);
		int y = (CAMERA_HEIGHT - height) / 2;
		final Camera camera = new Camera(0,y,CAMERA_WIDTH, height);
		EngineOptions options = new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED,
				new RatioResolutionPolicy(CAMERA_WIDTH, height), camera);
		return options;
	}

	@Override
	protected void onCreateResources() {
	}

	@Override
	protected Scene onCreateScene() {
//		AndEngineScene scene = new AndEngineScene(this);
//		TestScene scene = new TestScene(this);
		PuzzleScene scene = new PuzzleScene(this);
		return scene;
	}

	@Override
	protected int getLayoutID() {
		return R.layout.main_activity;
	}

	@Override
	protected int getRenderSurfaceViewID() {
		return R.id.renderSurfaceView;
	}


}
