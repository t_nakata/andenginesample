package com.example.andenginesample;

import android.graphics.BitmapFactory;
import android.graphics.Typeface;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.ui.activity.SimpleLayoutGameActivity;
import org.andengine.util.color.Color;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;

public abstract class BaseScene extends Scene {

    protected SimpleLayoutGameActivity mAct;
    private float FPS = 1f / 60;

    public BaseScene(SimpleLayoutGameActivity activity) {
        super();
        this.mAct = activity;
        // 画面生成処理
        onCreateScene();

        registerUpdateHandler(new TimerHandler(FPS, true, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                update();
            }
        }));
    }

    public abstract void onCreateScene();

    public abstract void update();

    /**
     * 指定したサイズより１つ上の2のべき乗の値を返す
     *
     * @param size
     * @return
     */
    protected int getTwoPowerSize(float size) {
        int value = (int) (size + 1);
        int pow2value = 64;
        while (pow2value < value)
            pow2value *= 2;
        return pow2value;
    }

    private BitmapFactory.Options getAssetOptions(String path) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        InputStream is = null;

        try {
            is = mAct.getResources().getAssets().open(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BitmapFactory.decodeStream(is, null, options);

        return options;
    }

    protected Sprite createSprite(String imgPath) {

        BitmapFactory.Options options = getAssetOptions(imgPath);

        // テクスチャーの生成
        BitmapTextureAtlas bta = new BitmapTextureAtlas(mAct.getTextureManager(), getTwoPowerSize(options.outWidth),
                getTwoPowerSize(options.outHeight), TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        mAct.getEngine().getTextureManager().loadTexture(bta);
        ITextureRegion btr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, mAct, imgPath, 0, 0);

        Sprite sprite = new Sprite(0, 0, btr, mAct.getVertexBufferObjectManager());
        sprite.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        sprite.setPosition(0, 0);
        // attachChild(sprite);
        return sprite;
    }

    protected Text createText(String text) {
        // テクスチャーの生成
        BitmapTextureAtlas bta = new BitmapTextureAtlas(mAct.getTextureManager(), 512, 512,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        // フォントをイニシャライズ
        Font font = new Font(mAct.getFontManager(), bta, Typeface.DEFAULT_BOLD, 50, true, Color.WHITE);
        // EngineのTextureManagerにフォントTextureを読み込む
        mAct.getTextureManager().loadTexture(bta);
        // FontManagerにフォントを読み込む
        mAct.getFontManager().loadFont(font);

        // テキストを作成
        return new Text(0,0 , font, text, 100, mAct.getVertexBufferObjectManager());
    }
}
