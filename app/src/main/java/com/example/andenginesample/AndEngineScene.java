package com.example.andenginesample;

import android.graphics.BitmapFactory;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.ui.activity.SimpleLayoutGameActivity;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;

public class AndEngineScene extends Scene {

	private static final String STAGE_BG = "img/stage_bg.png";
//	private static final String JELLYBEAN = "img/jellybean.png";
	private static final String CHARA_01 = "img/chara1.png";
	private SimpleLayoutGameActivity mActivity;
	private Sprite mSprite;

	private Sprite mBgSprite1;
	private Sprite mBgSprite2;

	public AndEngineScene(SimpleLayoutGameActivity activity) {
		super();
		this.mActivity = activity;

		setBgSprite();
		setJBSprite();
		registerUpdateHandler(timerHandler);
	}

	private void setBgSprite() {
		BitmapFactory.Options options = getAssetOptions(STAGE_BG);

		// テクスチャーの生成
		BitmapTextureAtlas bta = new BitmapTextureAtlas(
				mActivity.getTextureManager(),
				getTwoPowerSize(options.outWidth),
				getTwoPowerSize(options.outHeight),
				TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mActivity.getEngine().getTextureManager().loadTexture(bta);
		ITextureRegion btr = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(bta, mActivity, STAGE_BG, 0, 0);

		mBgSprite1 = new Sprite(0, 0, btr, mActivity.getVertexBufferObjectManager());
		mBgSprite1.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		mBgSprite1.setPosition(0, 0);
		attachChild(mBgSprite1);

		mBgSprite2 = new Sprite(0, 0, btr, mActivity.getVertexBufferObjectManager());
		mBgSprite2.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		mBgSprite2.setPosition(0, 0);
		attachChild(mBgSprite2);
	}

	private void setJBSprite() {
		BitmapFactory.Options options = getAssetOptions(CHARA_01);

		// テクスチャーの生成
		BitmapTextureAtlas bta = new BitmapTextureAtlas(
				mActivity.getTextureManager(),
				getTwoPowerSize(options.outWidth),
				getTwoPowerSize(options.outHeight),
				TextureOptions.BILINEAR_PREMULTIPLYALPHA);


		mActivity.getEngine().getTextureManager().loadTexture(bta);
		ITextureRegion btr = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(bta, mActivity, CHARA_01, 0, 0);

		mSprite = new Sprite(0, 0, btr, mActivity.getVertexBufferObjectManager());
		mSprite.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		mSprite.setPosition(0, 0);
		attachChild(mSprite);
	}

	private BitmapFactory.Options getAssetOptions(String path) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		InputStream is = null;

		try {
			is = mActivity.getResources().getAssets().open(path);
		} catch (IOException e) {
			e.printStackTrace();
		}

		BitmapFactory.decodeStream(is, null, options);

		return options;
	}

	/*
     * 1/60秒単位で呼ばれるTimerHandler
     */
    private TimerHandler timerHandler = new TimerHandler(1f / 60, true,
            new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {

                    // Spriteを上に移動させる
                    mSprite.setPosition(mSprite.getX(), mSprite.getY() - 5);

                    // Spriteが画面から完全に消えたら、画面最下部に移動させる
                    if (mSprite.getY() <= 0 - mSprite.getHeight()) {
                    	float posX = mSprite.getX();
                    	float posY = mActivity.getEngine().getCamera().getHeight() + mSprite.getHeight();
                        mSprite.setPosition(posX, posY);
                    }

                    float bgX = mBgSprite1.getX() - 2.0f;
                    if (bgX <= -mBgSprite1.getWidth()) {
                    	bgX = 0.0f;

                    }
                    mBgSprite1.setPosition(bgX, 0.0f);
                    mBgSprite2.setPosition(bgX + 959, 0.0f);

                }
            });


    /**
     * 指定したサイズより１つ上の2のべき乗の値を返す
     *
     * @param size
     * @return
     */
    private int getTwoPowerSize(float size) {
        int value = (int) (size + 1);
        int pow2value = 64;
        while (pow2value < value)
            pow2value *= 2;
        return pow2value;
    }
}
